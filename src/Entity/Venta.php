<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VentaRepository")
 */
class Venta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

   

    /**
     * @ORM\Column(type="integer")
     */
    private $IdProducto;
    /**
     * Get the value of IdProducto
     */ 
    public function getIdProducto()
    {
        return $this->IdProducto;
    }

    /**
     * Set the value of IdProducto
     *
     * @return  self
     */ 
    public function setIdProducto($IdProducto)
    {
        $this->IdProducto = $IdProducto;

        return $this;
    }

    

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Precio;

    /**
     * Get the value of Precio
     */ 
    public function getPrecio()
    {
        return $this->Precio;
    }

    /**
     * Set the value of Precio
     *
     * @return  self
     */ 
    public function setPrecio($Precio)
    {
        $this->Precio = $Precio;

        return $this;
    }

    

    /**
     * @ORM\Column(type="datetime")
     */
    private $FechaVenta;
    /**
     * Get the value of FechaVenta
     */ 
    public function getFechaVenta()
    {
        return $this->FechaVenta;
    }

    /**
     * Set the value of FechaVenta
     *
     * @return  self
     */ 
    public function setFechaVenta($FechaVenta)
    {
        $this->FechaVenta = $FechaVenta;

        return $this;
    }
     /**
     * @ORM\Column(type="integer")
     */
    private $IdCliente;

    /**
     * Get the value of IdCliente
     */ 
    public function getIdCliente()
    {
        return $this->IdCliente;
    }

    /**
     * Set the value of IdCliente
     *
     * @return  self
     */ 
    public function setIdCliente($IdCliente)
    {
        $this->IdCliente = $IdCliente;

        return $this;
    }
}
