<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClienteRepository")
 */
class Cliente
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100)
     */

    public $Num_Id;

     /**
     * @ORM\Column(type="string", length=100)
     */

    private $Nombre1;

     /**
     * @ORM\Column(type="string", length=100)
     */

    private $Nombre2;
    /**
     * @ORM\Column(type="string", length=100)
     */

    private $Apellido1;
     /**
     * @ORM\Column(type="string", length=100)
     */

    private $Apellido2;
    /**
    * @ORM\Column(type="string", length=100)
    */

    private $Dirreccion;
     /**
     * @ORM\Column(type="string", length=100)
     */
    
    private $Telefono;
     /**
     * @ORM\Column(type="string", length=100)
     */
    private $Genero;
   

    /**
     * Get the value of Num_Id
     */ 
    public function getNum_Id()
    {
        return $this->Num_Id;
    }

    /**
     * Set the value of Num_Id
     *
     * @return  self
     */ 
    public function setNum_Id($Num_Id)
    {
        $this->Num_Id = $Num_Id;

        return $this;
    }

    /**
     * Get the value of Nombre1
     */ 
    public function getNombre1()
    {
        return $this->Nombre1;
    }

    /**
     * Set the value of Nombre1
     *
     * @return  self
     */ 
    public function setNombre1($Nombre1)
    {
        $this->Nombre1 = $Nombre1;

        return $this;
    }

    /**
     * Get the value of Nombre2
     */ 
    public function getNombre2()
    {
        return $this->Nombre2;
    }

    /**
     * Set the value of Nombre2
     *
     * @return  self
     */ 
    public function setNombre2($Nombre2)
    {
        $this->Nombre2 = $Nombre2;

        return $this;
    }

    

    /**
     * Get the value of Apellido1
     */ 
    public function getApellido1()
    {
        return $this->Apellido1;
    }

    /**
     * Set the value of Apellido1
     *
     * @return  self
     */ 
    public function setApellido1($Apellido1)
    {
        $this->Apellido1 = $Apellido1;

        return $this;
    }

    /**
     * Get the value of Apellido2
     */ 
    public function getApellido2()
    {
        return $this->Apellido2;
    }

    /**
     * Set the value of Apellido2
     *
     * @return  self
     */ 
    public function setApellido2($Apellido2)
    {
        $this->Apellido2 = $Apellido2;

        return $this;
    }

    /**
     * Get the value of Dirreccion
     */ 
    public function getDirreccion()
    {
        return $this->Dirreccion;
    }

    /**
     * Set the value of Dirreccion
     *
     * @return  self
     */ 
    public function setDirreccion($Dirreccion)
    {
        $this->Dirreccion = $Dirreccion;

        return $this;
    }

    /**
     * Get the value of Telefono
     */ 
    public function getTelefono()
    {
        return $this->Telefono;
    }

    /**
     * Set the value of Telefono
     *
     * @return  self
     */ 
    public function setTelefono($Telefono)
    {
        $this->Telefono = $Telefono;

        return $this;
    }

    /**
     * Get the value of Genero
     */ 
    public function getGenero()
    {
        return $this->Genero;
    }

    /**
     * Set the value of Genero
     *
     * @return  self
     */ 
    public function setGenero($Genero)
    {
        $this->Genero = $Genero;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

 

    
}
