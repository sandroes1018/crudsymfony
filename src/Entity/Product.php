<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
    

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Nombre;
    /**
     * Get the value of Nombre
     */ 
    public function getNombre()
    {
        return $this->Nombre;
    }

    /**
     * Set the value of Nombre
     *
     * @return  self
     */ 
    public function setNombre($Nombre)
    {
        $this->Nombre = $Nombre;

        return $this;
    }
    

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Descripcion;
    /**
     * Get the value of Descripcion
     */ 
    public function getDescripcion()
    {
        return $this->Descripcion;
    }

    /**
     * Set the value of Descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($Descripcion)
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }
    

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Costo;
    /**
     * Get the value of Costo
     */ 
    public function getCosto()
    {
        return $this->Costo;
    }

    /**
     * Set the value of Costo
     *
     * @return  self
     */ 
    public function setCosto($Costo)
    {
        $this->Costo = $Costo;

        return $this;
    }
    

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Precio;
    /**
     * Get the value of Precio
     */ 
    public function getPrecio()
    {
        return $this->Precio;
    }

    /**
     * Set the value of Precio
     *
     * @return  self
     */ 
    public function setPrecio($Precio)
    {
        $this->Precio = $Precio;

        return $this;
    }
    

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Marca;
    /**
     * Get the value of Marca
     */ 
    public function getMarca()
    {
        return $this->Marca;
    }

    /**
     * Set the value of Marca
     *
     * @return  self
     */ 
    public function setMarca($Marca)
    {
        $this->Marca = $Marca;

        return $this;
    }
   
}
